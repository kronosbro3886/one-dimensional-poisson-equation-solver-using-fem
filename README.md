# One dimensional poisson equation solver using FEM

## Poisson equation

We are trying to solve the poisson equation

&Delta;*u=f*

&nabla;<sup>2</sup>u=f

The problem can be formulated in a weak form where

Finding a *u* such that *(u',v')=(f,v)* for all *v* &isin; *V*
 
We define a set of basis functions for a reduced function space and by putting each basis function in place of *v* we get an equation.
We solve for these set of equations to find the weights of the basis function in the required function u.

The function f can be defined in the cppsolver file.

The number of finite elements can be entered while running. For n finite elements we get n+2 coefficients. First and last elements are 0 and the middle elements are obtained.
