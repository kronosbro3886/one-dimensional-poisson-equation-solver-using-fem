#include <iostream>
#include <math.h>
#include <TNL/Matrices/SparseMatrix.h>
#include <TNL/Devices/Host.h>
#include <TNL/Containers/Vector.h>
#include <TNL/Solvers/Linear/Jacobi.h>
#include <TNL/Solvers/Linear/TFQMR.h>
/*
We solve the poisson equation of the form 
del(u)=f

where del() is the double derivative function
f is a given function and we need to solve for u

we try to find numerical solutions for the function u using linear lagrange elements

(u',p')=(f,p) (for all p belongs to same space as the solution required)
is a weak formulation of the original problem

using this formulation we use basis functions in place of p to obtain a series of linear equations solve them for finding the function u


here we solve for a function from 0 to 1

Au=B

*/
double f(double i)
{
    //Defining the function on the right of the equation used to find an array

    return i*cos(i);
}

double integrate(double i,double h)
{
    double dx=1e-6;
    double beg=i-h;
    double end=i+h;

    double sum=0;

    double cur=beg;

    while(cur<end)
    {
        double a1,a2;
        if(cur<i)
        {
            a1=(cur-beg)/(i-beg);
        }
        else
        {
            a1=(end-cur)/(end-i);
        }

        if((cur+dx)<i)
        {
            a2=(cur+dx-beg)/(i-beg);
        }
        else
        {
            a2=(end-cur-dx)/(end-i);
        }
        double val=dx*0.5*(f(cur)*a1+f(cur+dx)*a2);
        sum=sum+val;
        cur=cur+dx;
    }

    return sum;
}

int main()
{
    int n;
    std::cout<<"Enter the number of points between 0 and 1"<<'\n';
    std::cin>>n;

    double h=1/(1.0+n);

    

    std::vector <double> p;

    for(double i=0;i<=1;i=i+(1.0/n))
    {
        p.push_back(i);
    }

    std::map <std::pair<int,int>,double> A_map;
    TNL::Matrices::SparseMatrix<double, TNL::Devices::Host>  A(n+2,n+2);

    for(int i=1;i<=n;i++)
    {
        A_map[std::make_pair(i,i)]=2/h;
        A_map[std::make_pair(i,i-1)]=-1/h;
        A_map[std::make_pair(i,i+1)]=-1/h;
    }
    A_map[std::make_pair(0,0)]=1;
    A_map[std::make_pair(n+1,n+1)]=1;
    
    A.setElements(A_map);


    TNL::Containers::Vector<double,TNL::Devices::Host> b(n+2,0.0);
    std::vector <double> b_vector;

    b_vector.push_back(0);
    for(int i=1;i<=n;i++)
    {
        double val=integrate(double(i)/((double(n))+1),h);
        b_vector.push_back(val);
    }
    b_vector.push_back(0);

    std::cout<<b_vector.size()<<'\n';
    for(int i=0;i<=(n+1);i++)
    {
        b.setElement(i,b_vector[i]);
    }


    TNL::Containers::Vector<double,TNL::Devices::Host> x(n+2,1.0);
    auto matrix_ptr = std::make_shared< TNL::Matrices::SparseMatrix<double, TNL::Devices::Host> >(A);
    TNL::Solvers::Linear::TFQMR< TNL::Matrices::SparseMatrix<double, TNL::Devices::Host> > solver;
    solver.setMatrix( matrix_ptr );
    solver.setConvergenceResidue( 1.0e-6 );
    solver.solve( b, x );


    std::cout<<x<<'\n';
    return 0;
}